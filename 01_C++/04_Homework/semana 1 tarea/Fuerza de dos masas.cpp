

#include <iostream>
#include <math.h>
using namespace std;

int main(){
	
	float mass1;
	float mass2;
	float distance;
	double fuerza;
	
	mass1 = 0;
	mass2 = 0;
	distance = 0;
	fuerza = 0;
	
	cout<<"Digite la masa 1:";
	cin>>mass1;
	cout<<"Digite la masa 2:";
	cin>>mass2;
	cout<<"Digite la distancia:";
	cin>>distance;
	
	fuerza = (6.673 * 10^-8 * mass1 * mass2 )/distance^2;
	
	
	cout<<"La Fuerza de atraccion de las dos masas es: "<<fuerza;

	

	return 0;
}
	
	
